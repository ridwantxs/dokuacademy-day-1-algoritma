import java.util.*;
class LampuTombol {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Input: ");
        Integer N = Integer.parseInt(input.nextLine());
        Boolean onOff = false;
        for (var i = 1; i <= N; i++){
            if (N % i == 0) {
                onOff = !onOff;
            }
        }
        if(onOff == true) {
            System.out.println("Lampu Menyala");
        } else {
            System.out.println("Lampu Mati");
        }
        return;
    }
}
