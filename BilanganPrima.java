import java.util.*;
class BilanganPrima {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Input: ");
        Integer N = Integer.parseInt(input.nextLine());
        if(N <= 1) {
            System.out.print("Bukan Bilangan Prima");
            return;
        }
        for (var i = 2; i < N; i++){
            if (N % i == 0) {
                System.out.print("Bukan Bilangan Prima");
                return;
            }
        }
        System.out.print("Bilangan Prima");
        return;
    }
}
